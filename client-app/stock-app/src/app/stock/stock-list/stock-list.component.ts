import {Component, OnInit} from '@angular/core';
import {StockService} from "../stock.service";
import {Stock} from "../stock";

@Component({
  selector: 'app-stock-list',
  templateUrl: './stock-list.component.html',
  styleUrls: ['./stock-list.component.css'],
  providers: [StockService]
})
export class StockListComponent implements OnInit {

  private stocks: Stock[];

  constructor(private stockService: StockService) {
  }

  ngOnInit() {
    this.stockService.getStocks().subscribe(res => this.stocks = res);
  }

  deleteStock(id: number) {
    this.stockService.deleteStock(id).subscribe(() => this.stockService.getStocks().subscribe(res => this.stocks = res));
  }

  editStock(id: number) {
    let index = this.stocks.findIndex((stock) => stock.id === id);
    this.stocks[index].isEditable = !this.stocks[index].isEditable;
    this.stockService.editStock(this.stocks[index]).subscribe(res => {
      console.log(res);
    })
  }
}
