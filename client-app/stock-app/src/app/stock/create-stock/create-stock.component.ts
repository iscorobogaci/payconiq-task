import { Component, OnInit } from '@angular/core';
import {StockService} from "../stock.service";
import {Stock} from "../stock";
import {FormControl, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-create-stock',
  templateUrl: './create-stock.component.html',
  styleUrls: ['./create-stock.component.css'],
  providers:[StockService]
})
export class CreateStockComponent implements OnInit {

  stock: Stock;
  stockForm: FormGroup;

  constructor(private stockService: StockService) { }


  ngOnInit() {

    this.stockForm = new FormGroup({
      stockName: new FormControl('', Validators.required),
      stockPrice: new FormControl('', Validators.required),

    });
  }

  onSubmit() {
    if (this.stockForm.valid) {

        let stock: Stock = new Stock(null,
          this.stockForm.controls['stockName'].value,
          this.stockForm.controls['stockPrice'].value,
          new Date);
        this.stockService.save(stock).subscribe();

      }
      this.stockForm.reset();
    }

}
