import {Injectable} from '@angular/core';
import {Http, Headers, RequestOptions, RequestMethod, Response, HttpModule} from "@angular/http";

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import {Stock} from "./stock";
import {Observable} from "rxjs/Rx";

@Injectable({
  providedIn: 'root'
})
export class StockService {

  headers: Headers;
  options: RequestOptions;
  private apiURL = 'http://localhost:8080/api/stocks';

  constructor(private http: Http) {
    this.headers = new Headers({
      'Content-Type': 'application/json',
      'Accept': 'application/json'
    });
    this.options = new RequestOptions({
      headers: this.headers,
      withCredentials: true
    });
  }

  getStocks(): Observable<Stock[]> {
    return this.http.get(this.apiURL)
      .map((response) => response.json())
      .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }


  save(stock: Stock): Observable<any> {
    console.log('save');
    return this.http.post(this.apiURL,stock).catch((error:any)=>Observable.throw(error.json().error) || 'SERVER ERROR');
  }


  deleteStock(id: number): Observable<boolean> {
    console.log(id)
   return this.http.delete(this.apiURL + '/' + id)
      .map((res:Response) => true)

  }

  editStock(stock: Stock): Observable<Stock> {
    console.log('editing')
    console.log(this.apiURL + '/' + stock.id.toString())
    return this.http.put(this.apiURL + '/' + stock.id.toString(), stock)
      .map(res => res.json())
      .catch((error:any)=>Observable.throw(error.json().error) || 'SERVER ERROR');
  }
}
