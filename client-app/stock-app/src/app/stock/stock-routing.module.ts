import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {StockListComponent} from "./stock-list/stock-list.component";
import {CreateStockComponent} from "./create-stock/create-stock.component";

const routes: Routes = [
  {path:'stocks',component: StockListComponent},
  {path:'stocks/create',component: CreateStockComponent},
  {path:'stocks/edit/:id',component: CreateStockComponent},

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StockRoutingModule { }
