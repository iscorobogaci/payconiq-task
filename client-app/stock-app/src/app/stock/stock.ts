export class Stock {
  id: number;
  name: string;
  currentPrice: number;
  creationTimestamp: Date;
  public isEditable: boolean;

  constructor(id: number, name: string, currentPrice: number, creationTimestamp: Date) {
    this.id = id;
    this.name = name;
    this.currentPrice = currentPrice;
    this.creationTimestamp = creationTimestamp;
    this.isEditable = false;
  }

}
