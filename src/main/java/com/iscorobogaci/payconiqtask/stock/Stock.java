package com.iscorobogaci.payconiqtask.stock;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
public class Stock {

    @Id
    @GeneratedValue
    private Long id;

    private String name;

    private BigDecimal currentPrice;

    @CreationTimestamp
    private LocalDateTime creationTimestamp;


    public Stock(){
        super();
    }

    public Stock(Long id,String name, BigDecimal amount, LocalDateTime creationTimestamp) {
        super();
        this.id = id;
        this.name = name;
        this.currentPrice = amount;
        this.creationTimestamp = creationTimestamp;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getCurrentPrice() {
        return currentPrice;
    }

    public void setCurrentPrice(BigDecimal currentPrice) {
        this.currentPrice = currentPrice;
    }

    public LocalDateTime getCreationTimestamp() {
        return creationTimestamp;
    }

    public void setCreationTimestamp(LocalDateTime creationTimestamp) {
        this.creationTimestamp = creationTimestamp;
    }
}
