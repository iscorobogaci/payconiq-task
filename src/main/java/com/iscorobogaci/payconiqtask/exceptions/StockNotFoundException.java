package com.iscorobogaci.payconiqtask.exceptions;

public class StockNotFoundException extends RuntimeException {

    public StockNotFoundException(String exception) {
        super(exception);
    }
}
