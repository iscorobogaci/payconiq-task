package com.iscorobogaci.payconiqtask;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PayconiqTaskApplication {

	public static void main(String[] args) {
		SpringApplication.run(PayconiqTaskApplication.class, args);
	}
}
