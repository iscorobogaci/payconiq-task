package com.iscorobogaci.payconiqtask.service;

import com.iscorobogaci.payconiqtask.exceptions.StockNotFoundException;
import com.iscorobogaci.payconiqtask.repository.StockRepository;
import com.iscorobogaci.payconiqtask.stock.Stock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.math.BigDecimal;
import java.net.URI;
import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "*")
@RestController
public class StockResourceService {

    private StockRepository stockRepository;

    @Autowired
    public StockResourceService (StockRepository stockRepository){
        this.stockRepository = stockRepository;
    }

    @GetMapping("/api/stocks")
    public List<Stock> loadStocks() {
        return stockRepository.findAll();
    }

    @GetMapping("/api/stocks/{id}")
    public Stock loadStock(@PathVariable("id") Long id) {
        Optional<Stock> stock = stockRepository.findById(id);
        if (!stock.isPresent()) {
            throw new StockNotFoundException("id - " + id);
        }
        return stock.get();

    }

    @PostMapping("/api/stocks")
    public ResponseEntity<Object> createStock(@RequestBody Stock stock) {

        Stock savedStock = stockRepository.save(stock);

        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/api/stocks/{id}")
                .buildAndExpand(savedStock.getId())
                .toUri();

        return ResponseEntity.created(location).build();
    }

    @DeleteMapping("/api/stocks/{id}")
    public void deleteStock(@PathVariable("id") Long id) {
        stockRepository.deleteById(id);
    }


    @PutMapping("/api/stocks/{id}")
    public ResponseEntity<Stock> updateStock(@RequestBody Stock stock) {

        Optional<Stock> optionalStock = stockRepository.findById(stock.getId());

        if (!optionalStock.isPresent()) {
            return ResponseEntity.notFound().build();
        }

        optionalStock.get().setCurrentPrice(stock.getCurrentPrice());

        stockRepository.save(optionalStock.get());

        return ResponseEntity.noContent().build();

    }

}
