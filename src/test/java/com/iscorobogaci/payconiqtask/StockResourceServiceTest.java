package com.iscorobogaci.payconiqtask;

import com.iscorobogaci.payconiqtask.service.StockResourceService;
import com.iscorobogaci.payconiqtask.stock.Stock;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doNothing;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@RunWith(SpringRunner.class)
@WebMvcTest
public class StockResourceServiceTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private StockResourceService stockResourceService;

    private Stock stock;

    @Before
    public void setUp() {
        stock = new Stock();
        stock.setId(1L);
        stock.setCurrentPrice(BigDecimal.TEN);
        stock.setName("Nike Air Max");
        stock.setCreationTimestamp(LocalDateTime.now());
    }

    @Test
    public void testGetStocksSuccessfully() throws Exception {

        List<Stock> stocks = Collections.singletonList(stock);
        given(stockResourceService.loadStocks()).willReturn(stocks);
        mockMvc.perform(MockMvcRequestBuilders.get("/api/stocks"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].name", is(stock.getName())))
                .andExpect(jsonPath("$[0].currentPrice", is(stock.getCurrentPrice().intValue())))
                .andExpect(jsonPath("$[0].creationTimestamp", is(stock.getCreationTimestamp().toString())))
                .andExpect(jsonPath("$[0].id", is(stock.getId().intValue())))
                .andDo(print());
    }

    @Test
    public void testGetStockSuccessfully() throws Exception {
        given(stockResourceService.loadStock(stock.getId())).willReturn(stock);
        mockMvc.perform(MockMvcRequestBuilders.get("/api/stocks/{id}", stock.getId()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.name", is(stock.getName())))
                .andExpect(jsonPath("$.id", is(stock.getId().intValue())))
                .andExpect(jsonPath("$.currentPrice", is(stock.getCurrentPrice().intValue())))
                .andExpect(jsonPath("$.creationTimestamp", is(stock.getCreationTimestamp().toString())))
                .andDo(print());
    }

    @Test
    public void testDeleteStockSuccessfully() throws Exception {
        doNothing().when(stockResourceService).deleteStock(stock.getId());
        mockMvc.perform(MockMvcRequestBuilders.delete("/api/stocks/{id}", stock.getId()))
                .andExpect(status().isOk());
    }

}
