package com.iscorobogaci.payconiqtask;


import com.iscorobogaci.payconiqtask.repository.StockRepository;
import com.iscorobogaci.payconiqtask.stock.Stock;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class StockRepositoryTest {

    @Autowired
    private StockRepository stockRepository;

    @Test
    public void testCreateStock() {

        Stock savedStock = stockRepository.save(createTestStock());

        assertThat("We should get back created stock",
                savedStock,
                is(notNullValue()));

        assertThat("Created stock has been saved with wrong name",
                savedStock.getName(),
                is("Nike Air Max"));

        assertThat("Created stock has been saved with wrong price",
                savedStock.getCurrentPrice(),
                is(BigDecimal.TEN));

        assertThat("Created stock has been saved with wrong creationTimeStamp",
                savedStock.getCreationTimestamp(),
                is(LocalDateTime.of(2017, Month.FEBRUARY, 3, 6, 30, 40, 50000)));

    }

    @Test
    public void testLoadStocks() {

        stockRepository.save(createTestStock());
        List<Stock> listOfStocks = stockRepository.findAll();

        assertThat("Loaded stocks does not match the size",
                listOfStocks.size(),
                is(1));

    }

    @Test
    public void testUpdateStock() {

        Stock stock = stockRepository.save(createTestStock());
        Optional<Stock> retrievedStock = stockRepository.findById(stock.getId());

        if (retrievedStock.isPresent()) {
            retrievedStock.get().setCurrentPrice(BigDecimal.ONE);
            Stock updatedStock = stockRepository.save(retrievedStock.get());
            assertThat("Stock has not been updated properly",
                    updatedStock.getCurrentPrice(),
                    is(BigDecimal.ONE));
        }

    }


    private Stock createTestStock() {
        Stock stock = new Stock();
        stock.setCurrentPrice(BigDecimal.TEN);
        stock.setName("Nike Air Max");
        stock.setCreationTimestamp(LocalDateTime.of(2017, Month.FEBRUARY, 3, 6, 30, 40, 50000));
        return stock;
    }

}
